import axios from 'axios'

const serverError = 500

/**
 * Game API helper which has all the necessary routes
 */
class API {
  constructor() {
    this.http = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}/game`
    })
  }

  async create(id) {
    try {
      const { data } = await this.http.post(``)

      return data
    } catch (err) {
      return {
        status: serverError,
        message: err.response.data.message
      }
    }
  }

  async makeMove(id, position) {
    try {
      const { data } = await this.http.post(`/${id}/make-move`, {
        position
      })

      return data
    } catch (err) {
      return {
        status: serverError,
        message: err.response.data.message
      }
    }
  }
}

export default new API()
