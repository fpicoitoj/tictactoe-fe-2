import { GAME_MAKE_MOVE, GAME_CREATE } from './../types'
import api from './../../utils/api.js'

/**
 * Create a new game
 *
 * @param {function} callback
 */
export const createGame = () => async dispatch => {
  const payload = await api.create()

  dispatch({
    type: GAME_CREATE,
    payload
  })
}

/**
 * Make a move on the board
 *
 * @param {ObjectId} id
 * @param {number} pos
 * @param {function} callback
 */
export const makeMove = (id, pos) => async dispatch => {
  const payload = await api.makeMove(id, pos)

  dispatch({
    type: GAME_MAKE_MOVE,
    payload
  })
}
