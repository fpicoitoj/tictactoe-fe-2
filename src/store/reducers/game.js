import { GAME_MAKE_MOVE, GAME_CREATE } from '../types'

const INITIAL_STATE = {
  _id: null,
  board: null,
  isXPlaying: true,
  winner: null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GAME_CREATE:
      return {
        ...INITIAL_STATE,
        ...action.payload
      }

    case GAME_MAKE_MOVE:
      return {
        ...state,
        ...action.payload
      }

    default:
      return state
  }
}
