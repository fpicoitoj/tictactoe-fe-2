import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import Board from './components/Board'
import Piece from './components/Piece'
import { createGame, makeMove } from './store/actions'
import './style.css'

class App extends Component {
  constructor(props) {
    super(props)

    this.handleOnBoardClick = this.handleOnBoardClick.bind(this)
    this.handleOnStartAgainClick = this.handleOnStartAgainClick.bind(this)
  }

  componentDidMount() {
    this.props.createGame()
  }

  handleOnBoardClick(i) {
    this.props.makeMove(this.props.game._id, i)
  }

  handleOnStartAgainClick() {
    this.props.createGame()
  }

  render() {
    if (this.props.game._id === null) return <div>LOADING...</div>

    const { board, winner, isXPlaying } = this.props.game

    return (
      <div className="App">
        <div className="App__playing">
          Playing: <Piece value={isXPlaying ? 'X' : 'O'} />
        </div>
        <Board
          board={board}
          winner={winner}
          onBoardClick={this.handleOnBoardClick}
        />
        {winner !== null && (
          <Fragment>
            <div className="App__winner">
              The winner is: <Piece value={winner} />
            </div>
            <button
              type="button"
              className="App__play"
              onClick={this.handleOnStartAgainClick}
            >
              Play again
            </button>
          </Fragment>
        )}
      </div>
    )
  }
}

function mapStateToProps({ gameReducer }) {
  return {
    game: gameReducer
  }
}

export default connect(mapStateToProps, { createGame, makeMove })(App)
