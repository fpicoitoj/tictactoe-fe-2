import React, { PureComponent } from 'react'
import { X, O, DRAW } from './../../utils/consts'

/**
 * Quite simply render the appropriate piece (X or O) based on value args
 * isBig to increase the standard size to 2x
 */
class Piece extends PureComponent {
  render() {
    const { value, isBig } = this.props

    switch (value) {
      case X:
        return <i className={`fas fa-times ${isBig ? 'fa-2x' : ''}`} />
      case O:
        return <i className={`far fa-circle ${isBig ? 'fa-2x' : ''}`} />
      case DRAW:
        return 'draw'
      default:
        return null
    }
  }
}

export default Piece
