import React, { Component } from 'react'
import BoardSquare from './../BoardSquare'
import './style.css'

/**
 * Renders the board
 */
class Board extends Component {
  renderBoardSquares(startingIndex) {
    let returnArray = []
    for (let i = startingIndex; i < startingIndex + 3; i++) {
      returnArray.push(
        <BoardSquare
          value={this.props.board[i]}
          position={i}
          key={i}
          onClick={this.props.onBoardClick}
          disabled={this.props.winner !== null}
        />
      )
    }
    return returnArray
  }

  render() {
    return (
      <div className="board">
        <div className="board__row">{this.renderBoardSquares(0)}</div>
        <div className="board__row">{this.renderBoardSquares(3)}</div>
        <div className="board__row">{this.renderBoardSquares(6)}</div>
      </div>
    )
  }
}

export default Board
