import React, { PureComponent } from 'react'
import Piece from './../Piece'
import './style.css'

/**
 * Using Pure because this will do a shallow comparisson (primitives & references)
 * and thus if we used Component it would always render on each Board render
 * if we didn't check shouldComponentUpdate lifecycle manually
 * so all 9 buttons (board 3x3) would always be rendered.
 * In this case only each button will be rendered as their props change
 */
class BoardButton extends PureComponent {
  render() {
    const { value, position, onClick, disabled } = this.props

    return (
      <button
        type="button"
        className={`board__button`}
        onClick={() => onClick(position)}
        disabled={disabled}
      >
        <Piece value={value} isBig={true} />
      </button>
    )
  }
}

export default BoardButton
